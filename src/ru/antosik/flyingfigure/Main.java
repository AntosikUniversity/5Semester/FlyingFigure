package ru.antosik.flyingfigure;

import ru.antosik.flyingfigure.Forms.MainWindow;

public class Main {

    public static void main(String[] args) {
        MainWindow dialog = new MainWindow();
        dialog.setTitle("FlyingFigure");
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
