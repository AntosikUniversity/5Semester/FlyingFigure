package ru.antosik.flyingfigure.Models;

import ru.antosik.flyingfigure.Helpers.DirectionsX;
import ru.antosik.flyingfigure.Helpers.DirectionsY;
import ru.antosik.flyingfigure.Interfaces.IFlyingFigure;

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;

public class Ellipse implements IFlyingFigure {
    private final Ellipse2D shape = new Ellipse2D.Double();

    private Color color = Color.BLACK;
    private double size = 50;
    private Point2D position = new Point2D.Double(0, 0);

    private int speed = 50;
    private DirectionsX directionX = DirectionsX.RIGHT;
    private DirectionsY directionY = DirectionsY.BOTTOM;

    public Ellipse() {
    }

    @Override
    public Point2D move() {
        double newX = this.directionX == DirectionsX.LEFT ? position.getX() - speed : position.getX() + speed;
        double newY = this.directionY == DirectionsY.TOP ? position.getY() - speed : position.getY() + speed;

        return new Point2D.Double(newX, newY);
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }

    public Point2D getPosition() {
        return position;
    }

    public void setPosition(Point2D position) {
        this.position = position;
    }

    public void inverseDirectionX() {
        this.directionX = this.directionX == DirectionsX.LEFT ? DirectionsX.RIGHT : DirectionsX.LEFT;
    }

    public void inverseDirectionY() {
        this.directionY = this.directionY == DirectionsY.TOP ? DirectionsY.BOTTOM : DirectionsY.TOP;
    }

    @Override
    public void draw(Graphics2D g2d) {
        shape.setFrame(position.getX(), position.getY(), size, size);
        g2d.setColor(color);
        g2d.fill(shape);
        g2d.draw(shape);
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }
}
