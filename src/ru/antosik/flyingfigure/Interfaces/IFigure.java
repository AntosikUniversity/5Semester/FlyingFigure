package ru.antosik.flyingfigure.Interfaces;

import java.awt.*;
import java.awt.geom.Point2D;

public interface IFigure {
    Shape shape = null;
    Color color = Color.BLACK;
    double size = 50;

    Point2D position = new Point2D.Double(0, 0);

    void draw(Graphics2D g2d);
}
