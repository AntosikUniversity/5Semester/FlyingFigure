package ru.antosik.flyingfigure.Interfaces;

import ru.antosik.flyingfigure.Helpers.DirectionsX;
import ru.antosik.flyingfigure.Helpers.DirectionsY;

import java.awt.geom.Point2D;

public interface IFlyingFigure extends IFigure {
    int speed = 50;
    DirectionsX directionX = DirectionsX.RIGHT;
    DirectionsY directionY = DirectionsY.BOTTOM;

    Point2D move();
}
