package ru.antosik.flyingfigure.Components;

import ru.antosik.flyingfigure.Models.Ellipse;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Point2D;

public class DrawPanel extends JPanel {
    private static final int TIMER_UPDATE = 150;

    private Ellipse figure = new Ellipse();
    private Timer timer;

    public DrawPanel() {
        setBackground(Color.WHITE);
        setPreferredSize(new Dimension(600, 300));

        timer = new Timer(TIMER_UPDATE, e -> moveEllipse());
        timer.start();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;

        figure.draw(g2);
    }

    private void moveEllipse() {
        Point2D newCoords = figure.move();
        Dimension panelSize = this.getSize();

        double newX = newCoords.getX();
        double newY = newCoords.getY();

        double maxX = panelSize.width - figure.getSize();
        double maxY = panelSize.height - figure.getSize();

        if (newX <= 0) {
            newX = 0;
            figure.inverseDirectionX();
        } else if (newX >= maxX) {
            newX = maxX;
            figure.inverseDirectionX();
        }

        if (newY <= 0) {
            newY = 0;
            figure.inverseDirectionY();
        } else if (newY >= maxY) {
            newY = maxY;
            figure.inverseDirectionY();
        }

        figure.setPosition(new Point2D.Double(newX, newY));

        repaint();
    }

    public Color getEllipseColor() {
        return figure.getColor();
    }
    public void setEllipseColor(Color newColor) {
        figure.setColor(newColor);
    }
    public void setEllipseSize(int newSize) {
        figure.setSize(newSize);
    }
    public void setEllipseSpeed(int newSpeed) {
        figure.setSpeed(newSpeed);
    }
}
