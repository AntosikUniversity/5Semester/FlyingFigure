package ru.antosik.flyingfigure.Forms;

import com.intellij.uiDesigner.core.GridConstraints;
import ru.antosik.flyingfigure.Components.DrawPanel;

import javax.swing.*;
import java.awt.*;

public class MainWindow extends JDialog {
    private JPanel contentPane;
    private JButton selectColorButton;
    private JSlider sizeSlider;
    private JSlider speedSlider;
    private DrawPanel drawPanel;

    public MainWindow() {
        setPreferredSize(new Dimension(700, 500));
        setContentPane(contentPane);
        setModal(true);

        createUIComponents();
        initUIComponents();
    }

    private void initUIComponents() {
        selectColorButton.addActionListener(e -> drawPanel.setEllipseColor(JColorChooser.showDialog(this, "Choose pen color", drawPanel.getEllipseColor())));

        sizeSlider.addChangeListener(e -> {
            JSlider source = (JSlider) e.getSource();
            drawPanel.setEllipseSize(source.getValue());
        });

        speedSlider.addChangeListener(e -> {
            JSlider source = (JSlider) e.getSource();
            drawPanel.setEllipseSpeed(source.getValue());
        });
    }

    private void createUIComponents() {
        drawPanel = new DrawPanel();
        contentPane.add(drawPanel, new GridConstraints());
        pack();
    }
}
